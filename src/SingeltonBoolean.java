import android.R.bool;


public class SingeltonBoolean {

	private static SingeltonBoolean instance = null;
	
	private final boolean ja;
	
	private SingeltonBoolean(boolean ja) {
		this.ja = ja;
	}
	
	public static final synchronized SingeltonBoolean instance() {
		if(SingeltonBoolean.instance == null) {
			System.out.println("NICH DA");
			SingeltonBoolean.instance = new SingeltonBoolean(false);
			System.out.println("auf NEIN gesetzt!");
		}
		return SingeltonBoolean.instance;
			
	}

	public boolean isJa() {
		return ja;
	}
	
	public static void main(String[] args) {
		SingeltonBoolean sb = SingeltonBoolean.instance();
		System.out.println(sb.isJa());
		System.out.println(!sb.isJa());
	}
	
}
