package com.training.juliodroid;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.training.juliodroid.model.ISet;
import com.training.juliodroid.model.IText;
import com.training.juliodroid.model.OptionText;
import com.training.juliodroid.model.QuestionOptionSet;
import com.training.juliodroid.model.QuestionText;
import com.training.juliodroid.util.ParseUtils;

public class AddQuestionAndOptionsActivity extends Activity {
	
	private ParseUtils parseUtils;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_question_and_options);
		parseUtils = ParseUtils.getInstance();
	}
	
	public void checkEntriesAndPushToCloud(View view){
		boolean isEntryEmpty = checkEntriesNotEmpty();
		if(isEntryEmpty){
			// TODO : Focus on the not fiiled Elements or something
		}
		else{
			ISet questionOptionSet = this.createNewQuestionOptionSet();
			parseUtils.persistQuestionOptionSet(questionOptionSet);
			Intent intent = new Intent(this, FullscreenActivity.class);
			startActivity(intent);
			
		}
		
	}
	
	private ISet createNewQuestionOptionSet(){
		List<IText> options = new ArrayList<IText>();
		EditText editQuestion = (EditText) findViewById(R.id.edit_new_question);
		EditText editOption1 = (EditText) findViewById(R.id.edit_new_option_1);
		EditText editOption2 = (EditText) findViewById(R.id.edit_new_option_2);
		EditText editOption3 = (EditText) findViewById(R.id.edit_new_option_3);
		
		IText question = new QuestionText(editQuestion.getText().toString());
		IText option1 = new OptionText(editOption1.getText().toString());
		IText option2 = new OptionText(editOption2.getText().toString());
		IText option3 = new OptionText(editOption3.getText().toString());
		
		
		options.add(option1);
		options.add(option2);
		options.add(option3);
		
		ISet set = new QuestionOptionSet(question, options);
		
		
		return set
				;
		
		
	}
	
	private boolean checkEntriesNotEmpty(){
		boolean isEntryEmpty = false;
		EditText editQuestion = (EditText) findViewById(R.id.edit_new_question);
		EditText editOption1 = (EditText) findViewById(R.id.edit_new_option_1);
		EditText editOption2 = (EditText) findViewById(R.id.edit_new_option_2);
		EditText editOption3 = (EditText) findViewById(R.id.edit_new_option_3);
		
		if(editQuestion.getText().toString().isEmpty()){
			showEmptyEntryToast("Question is not filled");
			isEntryEmpty=true;
			}
		if(editOption1.getText().toString().isEmpty()){
			showEmptyEntryToast("Option 1 is not filled");
			isEntryEmpty=true;
		}
		if(editOption2.getText().toString().isEmpty()){
			showEmptyEntryToast("Option 2 is not filled");
			isEntryEmpty=true;
		}
		if(editOption3.getText().toString().isEmpty()){
			showEmptyEntryToast("Option 3 is not filled");
			isEntryEmpty=true;
		}
		
		return isEntryEmpty;
	}
	
	private void showEmptyEntryToast(String toastText){
		Context context = getApplicationContext();
		Toast toast = Toast.makeText(context, toastText, Toast.LENGTH_SHORT);
		toast.show();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.add_question_and_options, menu);
		return true;
	}

}
