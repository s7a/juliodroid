package com.training.juliodroid;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.training.juliodroid.util.ParseUtils;
import com.training.juliodroid.util.SystemUiHider;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 * 
 * @see SystemUiHider
 */
public class FullscreenActivity extends Activity {
	/**
	 * Whether or not the system UI should be auto-hidden after
	 * {@link #AUTO_HIDE_DELAY_MILLIS} milliseconds.
	 */
	private static final boolean AUTO_HIDE = true;

	/**
	 * If {@link #AUTO_HIDE} is set, the number of milliseconds to wait after
	 * user interaction before hiding the system UI.
	 */
	private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

	/**
	 * If set, will toggle the system UI visibility upon interaction. Otherwise,
	 * will show the system UI visibility upon interaction.
	 */
	private static final boolean TOGGLE_ON_CLICK = true;

	/**
	 * The flags to pass to {@link SystemUiHider#getInstance}.
	 */
	private static final int HIDER_FLAGS = SystemUiHider.FLAG_HIDE_NAVIGATION;

	/**
	 * The instance of the {@link SystemUiHider} for this activity.
	 */
	private SystemUiHider mSystemUiHider;
	
	/**
	 * This will help us to deal with all the Parse framework stuff
	 */
	private ParseUtils parseUtils;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fullscreen);

		// This is neede for my "parse" Cloud
		Parse.initialize(this, "dE2shHv5XRsQVy2rHNaEhGEZDxArgOkTT8XwRhEw",
				"1YKOCFYVk97UjAKQTdmL1VuuBU1GMunge77eGocY");
		
		// This is needed for the parse push-notification framework
		PushService.setDefaultPushCallback(this, FullscreenActivity.class);
		
		
		// We have only two users, so this is easy. Artur is only interested when Julia awnsers a question and Julia just needs to know when a new question option set was put online
		if(ParseInstallation.getCurrentInstallation().getInstallationId().equals(ParseUtils.PARSE_ARTUR_INSTALATION_ID)){
			PushService.subscribe(getApplicationContext(), ParseUtils.PARSE_OPTION_PUSH_CHANNEL, FullscreenActivity.class);
			
		} else{
			PushService.subscribe(getApplicationContext(), ParseUtils.PARSE_QUESTION_PUSH_CHANNEL, FullscreenActivity.class);
			
		}
		
		ParseInstallation.getCurrentInstallation().saveInBackground();
		
		
		
		parseUtils =  ParseUtils.getInstance();

		TextView question = (TextView) findViewById(R.id.question);
		question.setText(parseUtils.getQuestionText());

		
		
		
	
			final Button awnser1_button = (Button) findViewById(R.id.parse_button_1);
			awnser1_button.setText(parseUtils.getOptionText(0));

			final Button awnser2_button = (Button) findViewById(R.id.parse_button_2);
			awnser2_button.setText(parseUtils.getOptionText(1));

			final Button awnser3_button = (Button) findViewById(R.id.parse_button_3);
			awnser3_button.setText(parseUtils.getOptionText(2));
	
		
	
		
		final View controlsView = findViewById(R.id.fullscreen_content_controls);
		final View contentView = findViewById(R.id.fullscreen_content);

		// final Button awnser1_button = (Button)
		// findViewById(R.id.parse_button_1);
		// awnser1_button.setText("Testooooooo");

		// Set up an instance of SystemUiHider to control the system UI for
		// this activity.
		mSystemUiHider = SystemUiHider.getInstance(this, contentView,
				HIDER_FLAGS);
		mSystemUiHider.setup();
		mSystemUiHider
				.setOnVisibilityChangeListener(new SystemUiHider.OnVisibilityChangeListener() {
					// Cached values.
					int mControlsHeight;
					int mShortAnimTime;

					@Override
					@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
					public void onVisibilityChange(boolean visible) {
						if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
							// If the ViewPropertyAnimator API is available
							// (Honeycomb MR2 and later), use it to animate the
							// in-layout UI controls at the bottom of the
							// screen.
							if (mControlsHeight == 0) {
								mControlsHeight = controlsView.getHeight();
							}
							if (mShortAnimTime == 0) {
								mShortAnimTime = getResources().getInteger(
										android.R.integer.config_shortAnimTime);
							}
							controlsView
									.animate()
									.translationY(visible ? 0 : mControlsHeight)
									.setDuration(mShortAnimTime);
						} else {
							// If the ViewPropertyAnimator APIs aren't
							// available, simply show or hide the in-layout UI
							// controls.
							controlsView.setVisibility(visible ? View.VISIBLE
									: View.GONE);
						}

						if (visible && AUTO_HIDE) {
							// Schedule a hide().
							delayedHide(AUTO_HIDE_DELAY_MILLIS);
						}
					}
				});

		// Set up the user interaction to manually show or hide the system UI.
		contentView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (TOGGLE_ON_CLICK) {
					mSystemUiHider.toggle();
				} else {
					mSystemUiHider.show();
				}
			}
		});

		// Upon interacting with UI controls, delay any scheduled hide()
		// operations to prevent the jarring behavior of controls going away
		// while interacting with the UI.
		findViewById(R.id.dummy_button).setOnTouchListener(
				mDelayHideTouchListener);

	}
	
	
	
	// Next we want to implement a android dialog for the case one of the option was already chosen 
	// TODO : Implement the dialogs logic
	public void onOptionOneButtonPress(View view){
		boolean isQuestionAlreadyAwnsered = parseUtils.isLatestQuestionChosen();
		if(isQuestionAlreadyAwnsered){
			showDesicionAlreadyMadeAlertDialog();    			
		}else{
			this.showDesicionMadeDialog();
			
			final Button awnser1_button = (Button) findViewById(R.id.parse_button_1);
			String buttonContent = (String) awnser1_button.getText();
			parseUtils.choseOptionByText(buttonContent);
			
			
			
		}
		
	}
	public void onOptionTwoButtonPress(View view){
		boolean isQuestionAlreadyAwnsered = parseUtils.isLatestQuestionChosen();
		if(isQuestionAlreadyAwnsered){
			showDesicionAlreadyMadeAlertDialog();    			
		}
	else{
		this.showDesicionMadeDialog();
		final Button awnser1_button = (Button) findViewById(R.id.parse_button_2);
		String buttonContent = (String) awnser1_button.getText();
		parseUtils.choseOptionByText(buttonContent);
		
		
		
	}
		
	}
	public void onOptionThreeButtonPress(View view){
		boolean isQuestionAlreadyAwnsered = parseUtils.isLatestQuestionChosen();
		if(isQuestionAlreadyAwnsered){
			showDesicionAlreadyMadeAlertDialog();    			
		}
		else{
			this.showDesicionMadeDialog();
			final Button awnser1_button = (Button) findViewById(R.id.parse_button_3);
			String buttonContent = (String) awnser1_button.getText();
			parseUtils.choseOptionByText(buttonContent);
			
			
			
		}
		
	}
	
	public void onArturControlButtonPress(View view){
		AlertDialog arturoPWDialog = new AlertDialog.Builder(this).create();
		arturoPWDialog.setTitle("Nur Arturo darf in diesen Bereich");
		
		final EditText pwInput = new EditText(this);
		arturoPWDialog.setView(pwInput);
		
		arturoPWDialog.setButton("Okiii", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
            	String value =  pwInput.getText().toString();
            	if(value.equals("Arturo")){
                dialog.dismiss();
                createAndNavigateToQuestionOptionsActivity();
            	}
             }
          });
		
		arturoPWDialog.show();
		
		
	}
	
	private void createAndNavigateToQuestionOptionsActivity(){
		Intent intent = new Intent(this, AddQuestionAndOptionsActivity.class);
		
		startActivity(intent);
		
	}
	
	
	
	private void showDesicionAlreadyMadeAlertDialog(){
		AlertDialog alertDialog = new AlertDialog.Builder(this).create(); //Read Update
        alertDialog.setTitle("Nein, Nein, Nein .... so geht das nicht");
        alertDialog.setMessage("Die Entscheidung ist gefallen. Du musst deine Klickwut bis zur nächsten Runde unterdrücken");
        alertDialog.setButton("Wut unterdrücken", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
               dialog.dismiss();
            }
         });
        
        alertDialog.show();
		
	}
	private void showDesicionMadeDialog(){
		AlertDialog alertDialog = new AlertDialog.Builder(this).create(); //Read Update
		alertDialog.setTitle("Artur's Leute werden sich drum kümmern *GG*");
		alertDialog.setButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		
		alertDialog.show();
		
	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);

		// Trigger the initial hide() shortly after the activity has been
		// created, to briefly hint to the user that UI controls
		// are available.
		delayedHide(100);
	}

	/**
	 * Touch listener to use for in-layout UI controls to delay hiding the
	 * system UI. This is to prevent the jarring behavior of controls going away
	 * while interacting with activity UI.
	 */
	View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
		@Override
		public boolean onTouch(View view, MotionEvent motionEvent) {
			if (AUTO_HIDE) {
				delayedHide(AUTO_HIDE_DELAY_MILLIS);
			}
			return false;
		}
	};

	Handler mHideHandler = new Handler();
	Runnable mHideRunnable = new Runnable() {
		@Override
		public void run() {
			mSystemUiHider.hide();
		}
	};

	/**
	 * Schedules a call to hide() in [delay] milliseconds, canceling any
	 * previously scheduled calls.
	 */
	private void delayedHide(int delayMillis) {
		mHideHandler.removeCallbacks(mHideRunnable);
		mHideHandler.postDelayed(mHideRunnable, delayMillis);
	}
}
