package com.training.juliodroid.Constants;

public interface IGlobalConstants {
	
	
	/**
	 *  This id is used to pass the ParseUtils Object to the new intent, this was a failure because some Parse Objects are not serializable
	 */
	@Deprecated
	public static final String PARSE_UTIL_INTENT_ID = "paserseUtil";

}
