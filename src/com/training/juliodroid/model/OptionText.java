package com.training.juliodroid.model;

public class OptionText implements IText{

	private String text;
	
	public OptionText(String text){
		this.text=text;
	}
	
	@Override
	public String getText() {
		return this.text;
	}

	@Override
	public boolean isQuestion() {
		
		return false;
	}

}
