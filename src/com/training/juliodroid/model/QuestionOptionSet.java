package com.training.juliodroid.model;

import java.util.List;

public class QuestionOptionSet implements ISet {
	
	private IText question;
	private List<IText> options;
	
	
	public QuestionOptionSet(IText question, List<IText> options){
		this.question = question;
		this.options = options;
	}
	
	
	@Override
	public IText getQuestion() {
		return question;
	}
	
	@Override
	public List<IText> getOptions() {
		return options;
	}
	
	

}
