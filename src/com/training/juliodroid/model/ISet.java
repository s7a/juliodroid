package com.training.juliodroid.model;

import java.util.List;

public interface ISet {
	
	public IText getQuestion();
	
	public List<IText> getOptions();

}
