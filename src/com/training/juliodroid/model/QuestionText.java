package com.training.juliodroid.model;

public class QuestionText implements IText {
	
	private String text;
	
	public QuestionText(String text){
		this.text=text;
	}

	@Override
	public String getText() {
		return this.text;
	}

	@Override
	public boolean isQuestion() {
		return true;
	}
	
	

}
