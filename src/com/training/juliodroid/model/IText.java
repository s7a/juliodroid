package com.training.juliodroid.model;

public interface IText {
	
	public String getText();
	
	public boolean isQuestion();

}
