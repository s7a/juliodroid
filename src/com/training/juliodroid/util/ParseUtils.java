package com.training.juliodroid.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.SaveCallback;
import com.training.juliodroid.model.ISet;
import com.training.juliodroid.model.IText;

public class ParseUtils  implements Serializable{
	
	private static ParseUtils parseUtils;
	
	/**
	 * Generated serialUID
	 */
	private static final long serialVersionUID = -8399332220453905407L;
	
	
	final static String PARESE_QUESTION_OBJECT="Question";
	final static String PARESE_OPTION_OBJECT="Option";
	final static String PARESE_OPTION_ISCHOSEN_FIELD="isChosen";
	final static String PARESE_OPTION_TEXT_FIELD="text";
	final static String PARESE_QUESTION_TEXT_FIELD="Text";
	final static String PARESE_QUESTION_ISCHOSEN_FIELD="isChosen";
	
	public final static String PARSE_OPTION_PUSH_CHANNEL="Option";
	public final static String PARSE_QUESTION_PUSH_CHANNEL="Question";
	public final static String PARSE_ARTUR_INSTALATION_ID = "6d8e89f2-b801-41fb-8a4a-5ceb0072903a";
	private  ParseObject latestQuestion;
	private  List<ParseObject> options;
	
	private ParseUtils(){
		updateParseUtils();
	}
	
	private void updateParseUtils(){
		ParseQuery<ParseObject> questionQuery = new ParseQuery<ParseObject>(
				PARESE_QUESTION_OBJECT);
		
		questionQuery.orderByDescending("createdAt");
		questionQuery.setLimit(1);
		
		
		ParseQuery<ParseObject> optionQuery = new ParseQuery<ParseObject>(
				PARESE_OPTION_OBJECT);
		
		optionQuery.orderByDescending("createdAt");
		optionQuery.setLimit(3);
		
		try {
			List<ParseObject> result = questionQuery.find();
			latestQuestion = result.get(0);
			
			options = optionQuery.find();
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static ParseUtils getInstance(){
		if(null == parseUtils){
			synchronized (ParseUtils.class) {
				if(null == parseUtils){
					parseUtils = new ParseUtils();
				}
				
			}
		}
		return parseUtils;
	}
	
	
	
	
	public boolean isLatestQuestionChosen(){
		
		
		
		return latestQuestion.getBoolean(PARESE_QUESTION_ISCHOSEN_FIELD);
	
		
		
		
		
	}
	
	private ParseObject mapOptionToParseObjectByText(String text){
		for (ParseObject obj : options){
			if(obj.getString(PARESE_OPTION_TEXT_FIELD).equals(text)){
				return obj;
			}
		}
		
		return null;
	}
	
	public void choseOptionByText(String optionText){
		ParseObject chosenOption = this.mapOptionToParseObjectByText(optionText);
		chosenOption.put(PARESE_OPTION_ISCHOSEN_FIELD, true);
		latestQuestion.put(PARESE_QUESTION_ISCHOSEN_FIELD, true);
		
		this.persistParseObject(chosenOption);
		this.persistParseObject(latestQuestion);
		
		String questionText = latestQuestion.getString(PARESE_QUESTION_TEXT_FIELD);
		
		this.sendOptionPushNotification(questionText, optionText);
		
		
		
	}
	
	private void sendOptionPushNotification(String question, String option){
		StringBuilder text = new StringBuilder();
		text.append("Frage: ");
		text.append(question);
		text.append("/n");
		text.append("Antwort: ");
		text.append(option);
		
		this.sendChannelPushNotification(text.toString(), PARSE_OPTION_PUSH_CHANNEL);
		
	}
	
	private void sendQuestionPushNotification(){
		StringBuilder text = new StringBuilder();
		text.append("Neue spannende Entscheidungen !");
	
		
		this.sendChannelPushNotification(text.toString(), PARSE_QUESTION_PUSH_CHANNEL);
		
	}
	
	private void sendChannelPushNotification(String text, String channel){
		ParsePush push = new ParsePush();
		push.setChannel(channel);
		push.setMessage(text);
		try {
			push.send();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	private void persistParseObject(ParseObject obj){
			obj.saveInBackground(new SaveCallback() {
		          public void done(ParseException e) {
		                 if (e == null) {
		                   //myObjectSavedSuccessfully();
		                   System.out.println("EXCELENT");   

		                 } else {
		                   //myObjectSaveDidNotSucceed();
		                     System.out.println(e.getCause());
		                     System.out.println("VERY BAD");     
		                 }
		               }
		             });
	
	}
	
	public void persistQuestionOptionSet(ISet set){
		ParseObject newQuestion = new ParseObject(PARESE_QUESTION_OBJECT);
		newQuestion.put(PARESE_QUESTION_TEXT_FIELD	, set.getQuestion().getText());
		newQuestion.put(PARESE_QUESTION_ISCHOSEN_FIELD, false);
		
		this.persistParseObject(newQuestion);
		
		List<ParseObject> optionList = this.generateOptionParseObjects(set.getOptions());
		
		for(ParseObject obj : optionList){
			this.persistParseObject(obj);
		}
		
		
		this.updateParseUtils();
		
		this.sendQuestionPushNotification();
		
		
	}
	
	private List<ParseObject> generateOptionParseObjects(List<IText> options){
		List<ParseObject> objectList = new ArrayList<ParseObject>();
		for(IText opt : options){
			ParseObject newOption = new ParseObject(PARESE_OPTION_OBJECT);
			newOption.put(PARESE_OPTION_TEXT_FIELD, opt.getText());
			newOption.put(PARESE_OPTION_ISCHOSEN_FIELD, false);
			objectList.add(newOption);
			
		}
		
		return objectList;
		
	}
	
	public String getQuestionText(){
		return latestQuestion.getString(PARESE_QUESTION_TEXT_FIELD);
	}
	
	public String getOptionText(int optionNr){
		 return options.get(optionNr).getString(PARESE_OPTION_TEXT_FIELD);
	}

}
